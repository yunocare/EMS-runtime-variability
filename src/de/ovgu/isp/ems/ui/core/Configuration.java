package de.ovgu.isp.ems.ui.core;

import java.awt.Color;

/**
 * All selected features are mapped in this configuration class.
 * 
 * @author fs
 *
 */
public class Configuration {

	// Feature Names are spelled in upper case letters to improve feature
	// traceability
	private boolean THREEFA = true;
	private boolean TWOFA = false;
	private boolean APPLICATIONLOGGER = true;
	private boolean DE = false;
	private boolean EN = true;
	private Color THEMECOLOR = Color.RED;
	private int FONTSIZE = 12;
	private String FONTNAME = "Tahoma";
	private String BACKGROUNDIMAGE = "/Images/bk.jpg";

	public Configuration() {

	}

	public void setFeatureTHREEFA(boolean bool) {
		THREEFA = bool;
	}

	public boolean getFeatureTHREEFA() {
		return THREEFA;
	}

	public void setFeatureTWOFA(boolean bool) {
		TWOFA = bool;
	}

	public boolean getFeatureTWOFA() {
		return TWOFA;
	}

	public void setFeatureAPPLICATIONLOGGER(boolean bool) {
		APPLICATIONLOGGER = bool;
	}

	public boolean getFeatureAPPLICATIONLOGGER() {
		return APPLICATIONLOGGER;
	}

	public void setFeatureDE(boolean bool) {
		DE = bool;
	}

	public boolean getFeatureDE() {
		return DE;

	}

	public void setFeatureEN(boolean bool) {
		EN = bool;
	}

	public boolean getFeatureEN() {
		return EN;

	}

	public void setFeatureTHEMECOLOR(Color color) {
		THEMECOLOR = color;
	}

	public Color getFeatureTHEMECOLOR() {
		return THEMECOLOR;
	}

	public void setFeatureFONTSIZE(int size) {
		FONTSIZE = size;
	}

	public int getFeatureFONTSIZE() {
		return FONTSIZE;
	}

	public void setFeatureFONTNAME(String fontname) {
		FONTNAME = fontname;
	}

	public String getFeatureFONTNAME() {
		return FONTNAME;
	}

	public void setFeatureBACKGROUNDIMAGE(String image) {
		BACKGROUNDIMAGE = image;
	}

	public String getFeatureBACKGROUNDIMAGE() {
		return BACKGROUNDIMAGE;
	}

}
