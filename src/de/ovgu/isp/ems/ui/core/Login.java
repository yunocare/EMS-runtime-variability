package de.ovgu.isp.ems.ui.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.awt.event.KeyAdapter;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JPopupMenu;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JMenu;
import javax.swing.KeyStroke;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.Timer;
import javax.swing.JMenuBar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import de.ovgu.isp.ems.core.Localization;
import de.ovgu.isp.ems.core.Theme;
import de.ovgu.isp.ems.db.SQLiteDB;
import de.ovgu.isp.ems.db.Emp;
import org.apache.logging.log4j.Logger;
import javax.swing.DefaultComboBoxModel;
import de.ovgu.isp.ems.core.AccessControl;
import java.sql.SQLException;

/**
 * 
 * @author fs
 *
 */
public class Login extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;
	private Connection conn = null;
	private ResultSet rs = null;
	private PreparedStatement pst = null;

	/**
	 * @return selected user role needed to perform login
	 */
	public String getSelectedUserRole() {
		return txt_combo.getSelectedItem().toString();
	}

	// GEN-END:hook methods

	/**
	 * login construtors
	 */
	public Login() {

		initComponents();
		// get current DB connection:
		conn = SQLiteDB.java_db();

		// TODO: refactor this:
		Toolkit toolkit = getToolkit();
		Dimension size = toolkit.getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		currentDate();

	}

	/**
	 * This method initializes all UI components needed to load login jFrame
	 */
	private void initComponents() { // GEN-BEGIN:initComponents

		// Configuration object to get selected objects when needed:
		Configuration conf = new Configuration();

		if (FeatureAPPLICATIONLOGGING) {
			log.info("Start Program");
			log.info("initializing login UI components...");
		}
		/**
		 * login window configuration:
		 */
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);
		setTitle("Employee Management System");
		URL iconURL = getClass().getResource("/Images/Exit.png");
		ImageIcon icon = new ImageIcon(iconURL);
		setIconImage(icon.getImage());

		// dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));

		// JLabels:
		jLabel1 = new JLabel();
		jLabel2 = new JLabel();
		jLabel6 = new JLabel();
		jLabel4 = new JLabel();

		// JMenuItems:
		jMenuItem1 = new JMenuItem();
		jMenuItem2 = new JMenuItem();

		// JMenu:
		jMenu1 = new JMenu();
		jMenu3 = new JMenu();
		txt_date = new JMenu();
		lbl_time = new JMenu();
		jMenu2 = new JMenu();

		// JMenuBar:
		jMenuBar1 = new JMenuBar();

		// JRadioButton:
		jRadioButtonMenuItem1 = new JRadioButtonMenuItem();

		// JPopupMenu
		jPopupMenu1 = new JPopupMenu();

		// JPanel:
		jPanel2 = new JPanel();

		// JButtons:
		cmd_Login = new JButton();
		// JTextfields
		txt_username = new JTextField();
		txt_password = new JPasswordField();

		// set Text to Items:
		jMenuItem1.setText("jMenuItem1");
		jMenu1.setText("jMenu1");
		jRadioButtonMenuItem1.setText("jRadioButtonMenuItem1");
		jMenu2.setText(messages.getString("file"));
		// TODO: what is actually set to selected here? - in case: replace
		jRadioButtonMenuItem1.setSelected(true);

		jPanel2.setLayout(null);

		cmd_Login.setText(messages.getString("login"));

		cmd_Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				cmd_LoginActionPerformed(evt);
			}
		});
		jPanel2.add(cmd_Login);
		cmd_Login.setBounds(160, 470, 100, 30);

		// user fields:
		jLabel1.setForeground(new Color(255, 255, 255));
		jLabel1.setText(messages.getString("user") + ":");
		jPanel2.add(jLabel1);
		jLabel1.setBounds(10, 360, 100, 14);
		// password field:
		jLabel2.setForeground(new Color(255, 255, 255));
		jLabel2.setText(messages.getString("pwd") + ":");
		jPanel2.add(jLabel2);
		jLabel2.setBounds(10, 400, 100, 14);
		// TODO: set Font to Theme Feature
		jLabel6.setFont(new Theme().getThemeFont()); // NOI18N
		jLabel6.setForeground(new Color(255, 255, 255));
		// Enter login data field:
		jLabel6.setText(messages.getString("enterlogindata"));
		jPanel2.add(jLabel6);
		jLabel6.setBounds(10, 320, 390, 14); // previous: (10, 320, 241, 14)

		// Username text field
		jPanel2.add(txt_username);
		txt_username.setBounds(100, 350, 132, 30);

		// Listener:
		txt_password.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				passwordKeyPressedAction(evt);
			}
		});

		jMenuItem2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exitProgramAction(evt);
			}
		});

		jPanel2.add(txt_password);
		txt_password.setBounds(100, 390, 132, 30);

		/**
		 * Feature: THREEFA if feature is selected, additional field for user role will
		 * be added
		 */
		if (FeatureTHREEFA == true) {
			// get all listed user roles from feature AccessController:
			String[] roles = new AccessControl().getAllUserRoles();
			// set all user roles to JComboBox:
			txt_combo.setModel(new DefaultComboBoxModel(roles));
			jPanel2.add(txt_combo);
			txt_combo.setBounds(100, 430, 130, 30);
			jLabel3.setForeground(new Color(255, 255, 255));
			jLabel3.setText(messages.getString("selectrole") + ":");
			jPanel2.add(jLabel3);
			jLabel3.setBounds(10, 440, 100, 14);
		}

		jLabel4.setForeground(new Color(255, 255, 255));

		// set background:
		Theme t = new Theme();
		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(t.getThemeBackground()))); // NOI18N

		jPanel2.add(jLabel4);
		jLabel4.setBounds(0, 0, 790, 510);

		jMenuItem2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
		jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Exit.png"))); // NOI18N
		jMenuItem2.setText(messages.getString("exit"));

		jMenu2.add(jMenuItem2);

		jMenuBar1.add(jMenu2);
		jMenuBar1.add(jMenu3);

		jMenuBar1.add(txt_date);
		lbl_time.setText(new SimpleDateFormat("HH:mm:ss").format(new Date()));
		jMenuBar1.add(lbl_time);

		setJMenuBar(jMenuBar1);

		// GroupLayout
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, 788, GroupLayout.PREFERRED_SIZE)
						.addGap(0, 0, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, 509, GroupLayout.PREFERRED_SIZE)
						.addGap(0, 0, Short.MAX_VALUE)));
		// END GroupLayout

		pack();
	}

	// GEN-END:initComponents

	/**
	 * This method sets current Time Stamp as text to Menu
	 */
	public void currentDate() {

		Timer t = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Assumes clock is a custom component
				lbl_time.setText(new SimpleDateFormat("HH:mm:ss").format(new Date()));
			}
		});
		/**
		 * start Timer
		 */
		t.start();
		/**
		 * set Date Menu Text with local date format
		 */
		String dateString = new Localization().getCountrySpecificDate();
		txt_date.setText(dateString);
	}

	/**
	 * 
	 * @param evt
	 */
	private void cmd_LoginActionPerformed(ActionEvent evt) {// GEN-FIRST:event_cmd_LoginActionPerformed

		Configuration conf = new Configuration();
		// TODO: rename method

		if (FeatureAPPLICATIONLOGGING) {
			log.info("Login action triggered");
		}
		/**
		 * check if username field is empty
		 */
		if (txt_username.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_username_field"));
			if (FeatureAPPLICATIONLOGGING) {
				log.error("login failed due to empty user field.");
			}
		}
		/**
		 * check if password field is empty
		 */
		else if (txt_password.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_password_field"));
			if (FeatureAPPLICATIONLOGGING) {
				log.error("login failed due to empty password field.");
			}
		} else {
			/**
			 * Prepare SQL statement to check if credentials are valid
			 */
			String sql;
			/**
			 * if 3FA --> additional check if selected role matches user's role
			 */
			sql = new AccessControl().getLoginQuery();

			if (FeatureAPPLICATIONLOGGING) {
				log.info("checking login credetials....");
				log.info("preparing SQL query " + new AccessControl().getLoginQuery());
			}
			try {
				int count = 0; // Login-Status

				/**
				 * Prepare SQL statement: SELECT * FROM USERS WHERE input values are matching
				 */
				pst = conn.prepareStatement(sql);

				if (FeatureAPPLICATIONLOGGING) {
					log.info("DB connection established.");
				}
				pst.setString(1, txt_username.getText());
				pst.setString(2, txt_password.getText());

				/**
				 * FEATURE THREEFA
				 * 
				 * set selected user role to prepared sql statement to perform login with three
				 * factors
				 * 
				 */
				if (conf.getFeatureTHREEFA() == true) {
					try {
						pst.setString(3, txt_combo.getSelectedItem().toString()); // only if 3FA
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

				/**
				 * Execute Query
				 */
				rs = pst.executeQuery();
				while (rs.next()) {
					int id = rs.getInt(1);
					Emp.empId = id;
					count = count + 1; // count=1 : login allowed
				}

				String access = getSelectedUserRole();

				/**
				 * if user role is valid:
				 */
				if (access.toUpperCase().equals("ADMIN") || access.toUpperCase().equals("SALES")) {
					if (count == 1) {

						// JOptionPane.showMessageDialog(null, messages.getString("success"));
						MainMenu j = new MainMenu();
						j.setVisible(true);
						/**
						 * close login window:
						 */
						this.dispose();

						// TODO: refactor: to feature audit details:
						/**
						 * Create new Date object to store session time stamp in TABLE AUDITS
						 */
						Localization _loc = new Localization();
						/**
						 * insert session into database
						 */
						int value = Emp.empId;
						String reg = "insert into Audit (emp_id,date,status) values ('" + value + "','"
								+ _loc.getTimeString() + " / " + _loc.getDateString() + "','Logged in')";
						pst = conn.prepareStatement(reg);
						pst.execute();
						/**
						 * close login window
						 */
						this.dispose();
						if (FeatureAPPLICATIONLOGGING) {
							log.info("login successfully performed.");
							log.info("INSERT session into TABLE Audit Details.");
							log.info("Close login window");
							log.info("Open main menu window");
						}
					} else if (count > 1) {
						if (FeatureAPPLICATIONLOGGING) {
							log.error("login failed due to duplicate login.");
						}
						JOptionPane.showMessageDialog(null, messages.getString("duplicate_login"));
					} else {
						if (FeatureAPPLICATIONLOGGING) {
							log.error("login failed due to incorrect password.");
						}
						JOptionPane.showMessageDialog(null, messages.getString("incorrect_password"));
					}
				}
			} catch (Exception e) {
				if (FeatureAPPLICATIONLOGGING) {
					log.error("following exception caught: " + e.getStackTrace());
				}
				JOptionPane.showMessageDialog(null, e);
				e.printStackTrace();

			} finally {
				try {
					if (FeatureAPPLICATIONLOGGING) {
						log.info("close result set. close prepared sql statement.");
					}
					rs.close();
					pst.close();
				} catch (Exception e) {
					if (FeatureAPPLICATIONLOGGING) {

						log.error("While trying to close result set and prepared sql statement,"
								+ "following exception was thrown: " + e.getStackTrace());
					}
					e.printStackTrace();
				}
			}
		}
	}

	// GEN-LAST:event_cmd_LoginActionPerformed

	/**
	 * EXIT button -> ends program
	 * 
	 * @param evt
	 */
	private void exitProgramAction(ActionEvent evt) {
		if (FeatureAPPLICATIONLOGGING) {
			log.info("login window closed");
		}
		System.exit(0);
	}

	/**
	 * 
	 * @param evt
	 *            LoginButton pressed
	 */
	private void passwordKeyPressedAction(KeyEvent evt) {
		// TODO: rename method

		ActionEvent ae = new ActionEvent(evt.getSource(), evt.getID(), evt.paramString());
		// if Key = ENTER
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			/**
			 * call login logic:
			 */
			cmd_LoginActionPerformed(ae);
		}
	}

	/**
	 * @param args
	 *            no command line arguments
	 */
	public static void main(String args[]) {

		if (new Configuration().getFeatureAPPLICATIONLOGGER() == true) {
			log = new de.ovgu.isp.ems.logging.ApplicationLogger().getApplicationLogger();
		}
		/**
		 * Create and display login JFrame
		 */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Login().setVisible(true);
			}
		});
	}

	static Logger log;

	final boolean FeatureTHREEFA = new Configuration().getFeatureTHREEFA();
	final boolean FeatureTWOFA = new Configuration().getFeatureTWOFA();
	final boolean FeatureAPPLICATIONLOGGING = new Configuration().getFeatureAPPLICATIONLOGGER();

	private ResourceBundle messages = new Localization().getBundle();

	// JButtons:
	private JButton cmd_Login;
	// JMenu:
	private JMenu jMenu1;
	private JMenu jMenu2;
	private JMenu jMenu3;
	private JMenu lbl_time;
	private JMenu txt_date;
	private JMenuBar jMenuBar1;
	private JMenuItem jMenuItem1;
	private JMenuItem jMenuItem2;
	// JPanel:
	private JPanel jPanel2 = new JPanel();
	private JPopupMenu jPopupMenu1;
	private JRadioButtonMenuItem jRadioButtonMenuItem1;
	private JPasswordField txt_password;
	private JTextField txt_username;
	// JLabel:
	private JLabel jLabel1;
	private JLabel jLabel2;
	JLabel jLabel3 = new JLabel();
	private JLabel jLabel4;
	private JLabel jLabel6;
	private JComboBox txt_combo = new JComboBox(); // JComboBox: Select List

}
