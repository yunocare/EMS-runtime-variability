package de.ovgu.isp.ems.db;

import java.beans.FeatureDescriptor;
import java.io.File;
import java.sql.Connection;
import javax.swing.JOptionPane; //only required for testing db conn
import org.apache.logging.log4j.Logger;

import de.ovgu.isp.ems.logging.ApplicationLogger;
import de.ovgu.isp.ems.ui.core.Configuration;

import java.sql.DriverManager;


//TODO: !! DB list all table names and attributes

/**
 * Setting up connection to SQLite Database
 * 
 * The database itself is located inside the empnet.sqlite file.
 * 
 * Following TABLES are included in the database:
 * 
 * TABLE USERS
 * 
 * TABLE AUDIT_DETAIL
 * 
 * TABLE STAFF_INFORMATION
 * 
 * TABLE
 * 
 * 
 * @author fs
 *
 */
public class SQLiteDB {

	static Connection conn = null;

	/**
	 * This static method is used to get the current database connection.
	 * 
	 * @return DB connection to SQLite Database
	 */
	public static Connection java_db() {
		Logger log = null;
		if (FeatureAPPLICATIONLOGGER == true) {
			log = new ApplicationLogger().getApplicationLogger();
		}

		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager
					.getConnection("jdbc:sqlite:." + File.separator + "src" + File.separator + "empnet.sqlite");
			/*
			 * test the db connection:
			 * 
			 * JOptionPane.showMessageDialog(null, "Connection to database successfully"+
			 * "established.");
			 */

			if (FeatureAPPLICATIONLOGGER == true) {
				log.info("DB call - database connection established by user " + Emp.empId);
			}
			return conn;
		} catch (Exception e) {
			// display error message dialog:
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
			if (FeatureAPPLICATIONLOGGER == true) {
				log.fatal("Failed to open database connection to SQLite.");
				log.error(e.getStackTrace());
			}
			return null;
		}

	}

	// empty constructor:
	public SQLiteDB() {

	}

	static boolean FeatureAPPLICATIONLOGGER = new Configuration().getFeatureAPPLICATIONLOGGER();

}
